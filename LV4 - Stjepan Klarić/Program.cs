﻿using System;

namespace LV4___Stjepan_Klarić
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("Test1.csv");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] res = adapter.CalculateAveragePerRow(data);
            foreach (double a in res)
            {
                Console.WriteLine(a);
            }
            Console.WriteLine();
            double[] res2 = adapter.CalculateAveragePerColumn(data);
            foreach (double b in res2)
            {
                Console.WriteLine(b);
            }
        }
    }
}
