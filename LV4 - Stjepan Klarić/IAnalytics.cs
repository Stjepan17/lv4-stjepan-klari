﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4___Stjepan_Klarić
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
