﻿using System;
using System.Collections.Generic;
namespace LV_4___Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> lista = new List<IRentable>();
            Book knjiga = new Book("Družba Pere Kvržice");
            Video film = new Video("Law abiding citizen");
            lista.Add(knjiga);
            lista.Add(film);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(lista);
            printer.PrintTotalPrice(lista);



            HotItem item1 = new HotItem(new Book("Vlak u snijegu"));
            HotItem item2 = new HotItem(new Video("Shutter island"));
            lista.Add(item1);
            lista.Add(item2);
            printer.DisplayItems(lista);
            printer.PrintTotalPrice(lista);

            //Primjetim razliku u cijenu, dodan je bonus.



            List<IRentable> flashSale = new List<IRentable>();
            foreach (IRentable element in lista) {
                flashSale.Add(new DiscountedIteme(element, 50));
            }
            printer.DisplayItems(flashSale);
            printer.PrintTotalPrice(flashSale);
        }
    }
}
