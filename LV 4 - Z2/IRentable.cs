﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4___Z2
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
