﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4___Z2
{
    class DiscountedIteme : RentableDecorator
    {
        private readonly double HotItemDiscount;
        public DiscountedIteme(IRentable rentable, double discount) : base(rentable)  { this.HotItemDiscount = discount; }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() * (this.HotItemDiscount/100);
        }
        public override String Description
        {
            get
            {
                return "Trending: " + base.Description + " now at ["+ HotItemDiscount + "]% off";
            }
        }
    }
}
