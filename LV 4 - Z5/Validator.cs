﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4___Z5
{
    class Validator : IRegistrationValidator
    {
        private EmailValidator email;
        private PasswordValidator password;
        public Validator() {
            this.email = new EmailValidator();
            this.password = new PasswordValidator(7);
        }
        public bool IsUserEntryValid(UserEntry entry) {
            return email.IsValidAddress(entry.Email) && password.IsValidPassword(entry.Password);
        }
    }
}
