﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4___Z5
{
    interface IRegistrationValidator
    {
        bool IsUserEntryValid(UserEntry entry);
    }
}
