﻿using System;

namespace LV_4___Z5
{
    class Program
    {
        static void Main(string[] args)
        {
            PasswordValidator passwordValidator = new PasswordValidator(7);
            EmailValidator emailValidator = new EmailValidator();
            Console.WriteLine(passwordValidator.IsValidPassword("asasga"));
            Console.WriteLine(passwordValidator.IsValidPassword("12Affff"));
            Console.WriteLine(emailValidator.IsValidAddress("@gmail.com"));
            Console.WriteLine(emailValidator.IsValidAddress("bgmail@.net"));

            Validator validator = new Validator();
            while (!validator.IsUserEntryValid(UserEntry.ReadUserFromConsole())) ;


        }
    }
}
